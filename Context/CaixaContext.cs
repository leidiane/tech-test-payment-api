using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Context
{
  public class CaixaContext : DbContext
  {
    public CaixaContext(DbContextOptions<CaixaContext> options) : base(options)
    {

    }

    public DbSet<Venda> Vendas { get; set; }
  }
}