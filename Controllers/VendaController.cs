using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class VendaController : ControllerBase
  {
    private readonly CaixaContext _context;

    public VendaController(CaixaContext context)
    {
      _context = context;
    }

    [HttpPost]
    public IActionResult Criar(Venda venda)
    {
      _context.Add(venda);
      _context.SaveChanges();
      return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
    }

    [HttpGet("{id}")]
    public IActionResult ObterPorId(int id)
    {
      var vendaBanco = _context.Vendas.Find(id);

      if (vendaBanco == null)
        return NotFound();

      return Ok(vendaBanco);
    }

    [HttpPut("{id}")]
    public IActionResult Atualizar(int id, Venda venda)
    {
      var vendaBanco = _context.Vendas.Find(id);

      if (vendaBanco == null)
        return NotFound();

      vendaBanco.Status = venda.Status;

      _context.Vendas.Update(vendaBanco);
      _context.SaveChanges();

      return Ok(vendaBanco);
    }
  }
}